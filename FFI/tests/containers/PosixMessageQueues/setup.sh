#!/bin/bash

# shellcheck source=SCRIPTDIR/../../common.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/../../common.sh

make PREFIX=tst_sys

image=$(build_container_image gcc)
# shellcheck disable=SC2086
podman run $CONTAINER_PARAMS "$image" make
