# Freedom From Interference (FFI) - Pidbomb

## Title: Freedom from interference test with default PIDs

### Description:
Pidbomb test with default maximum available PIDs count of the container.
This test set helps investigate the pidbombing effect of exhausting PIDs, in other words it depletes available system resources.
The test checks: 
When available PIDs space exhausted:
* System outside was not affected by pidbomb run inside container
* Cannot spawn additional processes in the container 

1. `test_pidbomb_freedom_from_intereference` - check FFI created using basic pidbomb in C with default PIDs count for the container

### Test input
Test image: quay.io/centos/centos:stream9

### Expected output
```
...
-- Starting test ...
Compiling test for containers
...
Compile 'tst_pidbomb' in 'builder'
Copying 'tst_pidbomb' file
-- Running pidbomb test in 'bad_container'
PIDs limit is 'xxxx'
Initial PIDs of 'bad_container' is 'xx'
Running pidbomb
PIDs threshold is 'xx'
Maximum clones when the pidbomb was executed 'xxxx'
Create another process in 'bad_container'
Error: OCI runtime error: crun: fork: Resource temporarily unavailable
fail startup
PASS: Could not create new process in 'bad_container'
Create another process outside of 'bad_container'
System was not affected by pidbomb
Success!
Terminating..
builder
bad_container
```
