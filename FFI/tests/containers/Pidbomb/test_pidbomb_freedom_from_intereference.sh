#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running pidbomb test in '$BAD_CONTAINER'"
# checking the PIDs limit
get_pid_max() {
    # container value
    if [ $# -eq 0 ]; then
       pids=$(podman inspect "$BAD_CONTAINER" --format "{{.HostConfig.PidsLimit}}")
       # qm container has the PIDs maximum value stored in the file 
       if [ -z "$pids" ] || [ "$pids" -eq 0 ]; then
            if [ -f /sys/fs/cgroup/QM.slice/qm.service/pids.max ]; then
                pids=$(cat /sys/fs/cgroup/QM.slice/qm.service/pids.max)
                echo "PIDs limit from pids.max file: '$pids'" >&2
            else
                echo "Error: File not found" >&2
                return 1
            fi
       fi
       echo "$pids" 
    fi
}

pids_limit=$(get_pid_max "$@")
echo "PIDs limit is '$pids_limit'"

# checking initial PIDs of the container
pid_stats=$(podman stats --no-stream --format "{{.PIDs}}" "$BAD_CONTAINER")
echo "Initial PIDs of '$BAD_CONTAINER' is '$pid_stats'"

echo "Running pidbomb"
# path to the binary file is different in qm
# shellcheck disable=SC2154
if test -n "$FFI_QM_SCENARIO"; then
    # 2 because of 1 PID is occupied by PID1 shell process and
    # 1 PID is occupied by the main pidbomb process
    pid_script_path="/var/$file"; pid_threshold=$((pid_stats + 2)) 
    echo "PIDs threshold is '$pid_threshold'"
else
    # 1 PID is occupied by PID1 shell process (pid_stats) and
    # 1 PID is occupied by the main pidbomb process
    pid_script_path="./$file"; pid_threshold=$((pid_stats + 1))
    echo "PIDs threshold is '$pid_threshold'"
fi

# pidbomb prints number of clones created
num_clones=$(podman exec "$BAD_CONTAINER" "$pid_script_path")
echo "Maximum clones when the pidbomb was executed '$num_clones'"
# compare with pids_limit-pid_threshold
if [ "$num_clones" -ne "$((pids_limit-pid_threshold))" ]; then
    echo "Unexpected number of clones: $num_clones"
    exit 1
fi

echo "Create another process in '$BAD_CONTAINER'" 
# finished main pidbomb process replaced with podman sh process
if podman exec -it "$BAD_CONTAINER" sh -c "ls &>/dev/null"; then
    echo "ERROR: Could create new process in '$BAD_CONTAINER' although that should fail"
    exit 1
else
    echo "PASS: Could not create new process in '$BAD_CONTAINER'"
fi

echo "Create another process outside of '$BAD_CONTAINER'" 
# create 2 processes outside of the container, 1 process for finished main pidbomb process
if ! sh -c "ls | cat &>/dev/null"; then
    echo "Failed to create new process, system was affected by pidbomb"
    exit 1
else
    echo "System was not affected by pidbomb"
    echo "Success!"
    exit 0
fi
