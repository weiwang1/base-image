# Freedom From Interference (FFI) - SHM access restrictions

## Title: Shared Memory Test Suite

### Description:
This test set should investigate the isolation as well as access possibilities across container boundaries.
Included test cases:

1. `tc01_system-with-container-not-in-namespace` - SHM allocated in the system is inaccessible in containers
1. `tc02_system-with-container-in-namespace` - SHM allocated in the system is accessible in containers when sharing IPC namespace
1. `tc03_container-with-system-not-in-namespace` - SHM allocated in one container is inaccessible by the system
1. `tc04_container-with-system-in-namespace` - SHM allocated in one container is accessible by the system when sharing IPC namespace
1. `tc05_containers-not-in-namespace` - SHM allocated in one container is inaccessible in another container
1. `tc06_containers-in-namespace` - SHM allocated in one container is accessible in another container when sharing IPC namespace

### Test input
Test image: quay.io/centos/centos:stream9

### Expected output
1. `tc01_system-with-container-not-in-namespace (original result: fail)`
```
...
-- Starting test ...
...
Compiling test for system
Compiling test for containers
...
Compile 'tst_shm' in 'builder'
Copying 'tst_shm' file
-- Running shm. Failure expected: Test aborted, timeout!
-- Running shm test in bad_container. Failure expected: Test aborted, timeout!
Test is starting!
Hostname: localhost  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
Test is starting!
Hostname: xxxxxxxxxxxx  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
Test aborted, timeout!
Test aborted, timeout!
Caught signal! Exiting..
builder
bad_container_ipc_container_mode
bad_container_ipc_host_mode
orderly
bad_container
Terminating..
...
```
2. `tc02_system-with-container-in-namespace`
```
...
-- Starting test ...
...
Copying 'tst_shm' file
-- Running shm. Success expected: SHM access...
-- Running shm test in bad_container_ipc_host_mode. Success expected: SHM access...
Test is starting!
Hostname: localhost  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
Test is starting!
Hostname: xxxxxxxxxxxx  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
SHM access - value changed to: xxxxxxxxxx
SHM access - value changed to: xxxxxxxxxx
Terminating..
bad_container_ipc_container_mode
bad_container_ipc_host_mode
orderly
bad_container
```
3. `tc03_container-with-system-not-in-namespace (original result: fail)`
```
...
-- Starting test ...
...
Copying 'tst_shm' file
-- Running shm test in bad_container. Failure expected: Test aborted, timeout!
-- Running shm. Failure expected: Test aborted, timeout!
Test is starting!
Hostname: localhost  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
Test is starting!
Hostname: xxxxxxxxxxxx  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
Test aborted, timeout!
Test aborted, timeout!
Caught signal! Exiting..
bad_container_ipc_container_mode
bad_container_ipc_host_mode
orderly
bad_container
Terminating..
...
```
4. `tc04_container-with-system-in-namespace`
```
...
-- Starting test ...
...
Copying 'tst_shm' file
-- Running shm test in bad_container_ipc_host_mode. Success expected: SHM access...
-- Running shm. Success expected: SHM access...
Test is starting!
Hostname: localhost  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
Test is starting!
Hostname: xxxxxxxxxxxx  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
SHM access - value changed to: xxxxxxxxxx
SHM access - value changed to: xxxxxxxxxx
Terminating..
bad_container_ipc_container_mode
bad_container_ipc_host_mode
orderly
bad_container
```
5. `tc05_containers-not-in-namespace (original result: fail)`
```
...
-- Starting test ...
...
Copying 'tst_shm' file
-- Running shm test in orderly. Failure expected: Test aborted, timeout! 
-- Running shm test in bad_container. Failure expected: Test aborted, timeout!
Test is starting!
Hostname: xxxxxxxxxxxx  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
Test is starting!
Hostname: xxxxxxxxxxxx  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
Test aborted, timeout!
Test aborted, timeout!
Caught signal! Exiting..
bad_container_ipc_container_mode
bad_container_ipc_host_mode
orderly
bad_container
Terminating..
...
```
6. `tc06_containers-in-namespace`
```
...
-- Starting test ...
...
Copying 'tst_shm' file
-- Running shm test in orderly. Success expected: SHM access...
-- Running shm test in bad_container_ipc_container_mode. Success expected: SHM access...
Test is starting!
Hostname: xxxxxxxxxxxx  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
Test is starting!
Hostname: xxxxxxxxxxxx  |  IPC name: /memtest  |  Initial value: xxxxxxxxxx
SHM access - value changed to: xxxxxxxxxx
SHM access - value changed to: xxxxxxxxxx
Terminating..
bad_container_ipc_container_mode
bad_container_ipc_host_mode
orderly
bad_container
```
