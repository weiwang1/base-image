#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running shm. Success expected: SHM access..."
./tst_sys_shm &

printf "%s\n" "-- Running shm test in $cntr_ipc_host. Success expected: SHM access..."
podman exec "$cntr_ipc_host" ./tst_shm
