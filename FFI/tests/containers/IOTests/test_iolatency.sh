#!/bin/bash
# shellcheck source=SCRIPTDIR/setup_iotests.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup_iotests.sh

# Run orderly without interference 
echo "Starting first run"
# shellcheck disable=SC2086
podman exec -it orderly ioping $IOPING_OPTS /test > run1.log

# Run confusion to start interference
echo "Starting interference using: $STRESS_CMD"
# shellcheck disable=SC2086
podman run --detach $CONFUSION_OPTS -v /var/test_confusion:/test:Z --workdir /test --name confusion "$test_image" $STRESS_CMD > /dev/null

# Give stress-ng a moment to start
sleep 10

# Run orderly with interference
echo "Starting second run"
# shellcheck disable=SC2086
podman exec -it orderly ioping $IOPING_OPTS /test > run2.log

# Cleanup containers and directories
podman rm --force orderly > /dev/null
podman rm --force confusion > /dev/null
rm -rf /var/test_orderly
rm -rf /var/test_confusion

# Process ioping output
avg1=$(cut -d ' ' -f6 < run1.log)
avg2=$(cut -d ' ' -f6 < run2.log)
mdev1=$(cut -d ' ' -f8 < run1.log)
mdev2=$(cut -d ' ' -f8 < run2.log)

# Sanity check
if [ "$mdev1" -gt "$avg1" ] || [ "$mdev2" -gt "$avg2" ]; then
  error "Standard deviation larger than average! Aborting.."
  exit 1
fi

# Interference presence check
max_rel_error=$(( avg1 * 10 ))
avg_difference=$(( avg1 - avg2 ))
avg_difference=${avg_difference#-}  # abs()
echo "First  run I/O latency is $((avg1 / 1000))us"
echo "Second run I/O latency is $((avg2 / 1000))us"
if [ "$avg_difference" -gt "$max_rel_error" ]; then
    error "Interference is present!"
    exit 1
else
    success "No interference!"
fi


