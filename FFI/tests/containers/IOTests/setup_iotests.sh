#!/bin/bash

# shellcheck source=SCRIPTDIR/../../common.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/../../common.sh

# Process program options
while test $# -gt 1; do
  case $1 in
    -s|--stress-cmd)
      STRESS_CMD="$2"
      shift 2
      ;;
    -a|--container-opts)
      ORDERLY_OPTS="$ORDERLY_OPTS $2"
      CONFUSION_OPTS="$CONFUSION_OPTS $2"
      shift 2
      ;;
    -o|--orderly-opts)
      ORDERLY_OPTS="$ORDERLY_OPTS $2"
      shift 2
      ;;
    -c|--confusion-opts)
      CONFUSION_OPTS="$CONFUSION_OPTS $2"
      shift
      ;;
  esac
done

if [ -z "$STRESS_CMD" ]; then
  error "No stress command given!"
  exit 1
fi

ioping="http://ftp.halifax.rwth-aachen.de/fedora-epel/8/Everything/$(uname -m)/Packages/i/ioping-1.1-1.el8.$(uname -m).rpm"
test_image=$(build_container_image stress-ng "$ioping")
IOPING_OPTS="-B -c 5 -i 1 -q -S 100m"
export IOPING_OPTS # exporting to avoid shellcheck error

# Find a block device that podman runs on
PODMAN_ROOT=$(df --output=source "$(podman info --format '{{ .Store.GraphRoot }}')" | tail -n 1)
if [[ $PODMAN_ROOT == /dev/* ]]; then
    DEVICE=$(lsblk -no pkname "$PODMAN_ROOT")
else
    error "Not a block device"
    exit 1
fi

# Change I/O scheduler to bfq to allow I/O operations limitation
echo bfq > /sys/block/"${DEVICE}"/queue/scheduler

# Make new temporary directories to run ioping and stress-ng
mkdir -p /var/test_orderly
mkdir -p /var/test_confusion

# Options that mitigate the interference
MITIGATION_OPTS="$CONFUSION_OPTS --device-write-iops=/dev/$DEVICE:10"
MITIGATION_OPTS="$MITIGATION_OPTS --device-read-iops=/dev/$DEVICE:10"
MITIGATION_OPTS="$MITIGATION_OPTS --blkio-weight=11"

# shellcheck disable=SC2086
podman run --detach $CONTAINER_PARAMS $ORDERLY_OPTS -v /var/test_orderly:/test:Z --name orderly "$test_image" > /dev/null

