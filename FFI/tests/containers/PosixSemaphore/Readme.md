# Freedom From Interference (FFI) - Posix Semaphore

## Title: Posix Semaphore Test Suite

### Description:
This test set should investigate the PosixSemaphore synchronization mechanism across container boundaries.
Included test cases:

1. `test_01_server_client_parallel_instances_in_system` - server and client created in the system are synchronized successfully
1. `test_02_server_client_parallel_instances_in_system_and_container` - server created in the container is not able to be accessed by client in the system with the same semaphore
1. `test_03_server_client_parallel_instances_in_containers_different_namespaces` - server created in the container is not able to be accessed by the client container with different namespace
1. `test_04_server_client_parallel_instances_in_containers_the_same_namespace` - server created in the container is able to be accessed by the client container with same namespace

### Test input
Test image: quay.io/centos/centos:stream9

Semaphore name: sem_name

Timeout: 5

### Expected output
1. `test_01_server_client_parallel_instances_in_system`
```
...
-- Starting test ...
Compiling test for system
Compiling test for containers
...
Compile 'tst_named_posix_semaphore' in 'builder'
Copying 'tst_named_posix_semaphore' file
-- Running named_posix_semaphore as server.
-- Running named_posix_semaphore as client.
--- Client mode ---
Success!
--- Server mode ---
Success!
Terminating..
builder
bad_container
```
2. `test_02_server_client_parallel_instances_in_system_and_container (original result: fail)`
```
...
-- Starting test ...
Copying 'tst_named_posix_semaphore' file
-- Running named_posix_semaphore test as a server (detached mode with bad_container).
-- Running named_posix_semaphore as a client in system. Expected: failure, not able to access the same semaphore.
Error: Timed out waiting for semaphore. Connection timed out 
--- Server mode ---
Caught signal! Exiting..
bad_container
Terminating..
...
```
3. `test_03_server_client_parallel_instances_in_containers_different_namespaces (original result: fail)`
```
...
-- Starting test ...
Copying 'tst_named_posix_semaphore' file
-- Running named_posix_semaphore test in server container in bad_container.
-- Running named_posix_semaphore test in client container. Expected: failure.
--- Server mode ---
Error: Timed out waiting for semaphore. Connection timed out
Caught signal! Exiting..
bad_container
Terminating..
...
```
4. `test_04_server_client_parallel_instances_in_containers_the_same_namespace`
```
...
-- Starting test ...
Copying 'tst_named_posix_semaphore' file
-- Running named_posix_semaphore test in server container in bad_container.
-- Running named_posix_semaphore test in client container using the same namespace as a server in bad_container. Expected: success.
Terminating..
bad_container
```

### Results location:

output.txt.
