#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <semaphore-name> <timeout-seconds>"
    exit 1
fi

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running named_posix_semaphore as server."
./tst_sys_named_posix_semaphore "$1" "$2" &

sleep 2

printf "%s\n" "-- Running named_posix_semaphore as client."
./tst_sys_named_posix_semaphore "$1" "$2"
