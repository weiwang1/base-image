#!/bin/bash

# shellcheck source=SCRIPTDIR/setup.sh
. "$(dirname "$(readlink --canonicalize "$0")")"/setup.sh

printf "%s\n" "-- Running system_v_semaphore in server container in $BAD_CONTAINER."
# shellcheck disable=SC2154
podman exec -d $BAD_CONTAINER bash -c "$(get_path_to_script "$file")" > /dev/null

printf "%s\n" "-- Running system_v_semaphore in client container. Expected: failure, not able to access the same semaphore."
# shellcheck disable=SC2154
podman exec "$cntr_client" ./tst_system_v_semaphore
