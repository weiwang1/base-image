#!/bin/bash

set -e

PACKAGE_TO_CHECK="tmux"
VERSION_FILE="/var/.distro_version"

# Helper functions

is_before_reboot() {
  # 'TMT_REBOOT_COUNT = 0' means it's the first boot
  # 'TMT_REBOOT_COUNT > 0' means the system has already reboot
  if [ "$TMT_REBOOT_COUNT" -eq 0 ]; then
    echo "True"
  elif [ "$TMT_REBOOT_COUNT" -eq 1 ]; then
    echo "False"
   else
    echo "ERROR: Something went wrong. It's not possible to know if the system has rebooted"
    exit 1
  fi
}

is_the_repo_available() {
  curl --output /dev/null --silent --head --fail "${REPO_URL}/config"
}

is_installed() {
  local package="$1"

  if rpm -q "$package" > /dev/null ; then
    return 0
  else
    return 1
  fi
}

distro_version() {
  rpm-ostree status --json > status.json
  version="$(jq '.deployments[] | select(.booted == true) | .version' status.json)"
  echo "${version}"
}

check_file_exists_and_not_empty() {
  local filename="$1"

  if [[ ! -f "$filename" ]]; then
      echo "Error: the file '${filename}' does not exist"
      return 1
  elif [[ ! -s "$filename" ]]; then
      echo "Error: the file '${filename}' exists but is empty"
      return 1
  fi

  return 0
}

is_different_version() {
  local new_version="$1"
  local old_version

  if ! check_file_exists_and_not_empty "${VERSION_FILE}"; then
    echo "ERROR: the old distro version does not exist"
    exit 1
  fi

  old_version="$(< ${VERSION_FILE})"

  echo "Original version: ${old_version}"
  echo "Current version: ${new_version}"

  if [[ "${old_version}" != "${new_version}" ]]; then
    return 0
  else
    return 1
  fi
}


# Check if it's possible to run the test

if [[ ! -v REPO_URL ]]; then
  echo "ERROR: The variable REPO_URL is not defined"
  exit 1
fi

if ! is_the_repo_available ; then
  echo "ERROR: The OSTree repo is not available"
  exit 1
fi


# The actual test

if [[ "$(is_before_reboot)" == "True" ]] ; then
  echo ">> Before the reboot <<"

  echo "[*] Checking the original remote URL:"
  ostree remote list --show-urls

  echo -e "\n[*] Updating the default remote URL to '${REPO_URL}':"
  ostree remote add --force --no-gpg-verify auto-sig "${REPO_URL}"

  echo -e "\n[*] Checking that the remote URL actually changed:"
  ostree remote list --show-urls

  echo -e "\n[*] Checking that the package ${PACKAGE_TO_CHECK} is NOT installed:"
  if is_installed "${PACKAGE_TO_CHECK}" ; then
    echo "FAIL: The package '${PACKAGE_TO_CHECK}' is installed"
    exit 1
  else
    echo "PASS: The package '${PACKAGE_TO_CHECK}' is NOT installed"
  fi

  echo -ne "\n[*] Checking the distro version: "
  distro_version | tee "${VERSION_FILE}"

  echo -e "\n[*] Performing the upgrade from the remote OSTree repo ('rpm-ostree upgrade')"
  rpm-ostree upgrade

  echo -e "\n[*] Rebooting..."
  tmt-reboot

else
  echo ">> After the reboot <<"

  echo -e "\n[*] Checking that the package is installed:"
  if is_installed "${PACKAGE_TO_CHECK}" ; then
    echo "PASS: The package '${PACKAGE_TO_CHECK}' is installed"
  else
    echo "FAIL: The package '${PACKAGE_TO_CHECK}' is NOT installed"
    exit 1
  fi

  echo -e "\n[*] Checking the new distro version:"
  if is_different_version "$(distro_version)" ; then
    echo "PASS: The versions are different"
  else
    echo "FAIL: The versions are the same"
    exit 1
  fi
fi
