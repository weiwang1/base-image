# OSTree Image Update

This test verifies if an Automotive OSTree-based image can be updated using a remote
OSTree repository with updates.

Follow the instructions in this document to perform the update:
[ostree-image-update](https://sigs.centos.org/automotive/building/updating_ostree/)

## Prerequisites

This test should be run after the `plans/create-ostree-repo` TMT plan from the
[create-osbuild](https://gitlab.com/redhat/edge/ci-cd/pipe-x/create-osbuild) repository.

That job will create a new OSTree commit based on the one used to build the image
we're testing. After the job finishes, an OSTree repository will be available at the job's
`ARTIFACTS_URL`, which can be used as input (`REPO_URL`) for this test.

The job that builds the new updated OSTree repository will run in the CI pipeline before
triggering this test, ensuring that the OSTree repository is an update from the image
being tested.

## Test Suite Steps

1. Check the distribution version and ensure a specific package is not installed.
2. Set the rpm-ostree remote and upgrade.
3. Reboot the system.
4. Verify the expected changes after the reboot:
    - The package is now installed.
    - The distribution version should be different.

## Running Tests with TF Provisioned AutoSD OSTree based Image

After running the TMT job that creates the OSTree repository for your image, you'll need
to pass the AMI name (which can be found [here](https://rhivos.auto-toolchain.redhat.com/in-vehicle-os-9/nightly/sample-images/AMI_info_qa_aarch64.json) for the nightly) as the `compose` and
the `TMT_DATA` directory at the `ARTIFACTS_URL` from that job as the `REPO_URL`.

```shell
testing-farm request \
             --arch x86_64 \
             --compose auto-osbuild-qemu-autosd9-qa-ostree-x86_64-1287644736.dd30634c \
             --git-url https://gitlab.com/redhat/edge/tests/base-image \
             --plan /ostree-update/plans/remote-update \
             -e REPO_URL=https://artifacts.dev.testing-farm.io/a5870644-155f-4456-95b6-f677abb1077d/work-create-ostree-repoi5slxg3t/plans/create-ostree-repo/data/ostree-repo/
```

> **NOTE:** The `REPO_URL` and the `compose` values are just examples. They might not exist by the time you're reading this.

## Running Tests with Qcow and Local Repository

Please refer to ostree-image-update.
The documentation suggests using --publish-dir="<local-repo-url>" while running the runvm script.