#!/bin/bash

set -ux

# Check the signed enabled in /usr/lib/ostree/prepare-root.conf after install the sample-image.
INITRAMFS_FILE=(/boot/ostree/"$OSTREE_OS_NAME"-*/initramfs-"$(uname -r)".img)
CONF_FILE=usr/lib/ostree/prepare-root.conf

# This feature only for ostree image.
if [ ! -e /run/ostree-booted ]; then
   echo "Non-ostree images cannot be used for this test!"
   exit 1
fi

# create template folder and cd into
mkdir /tmp/initrd && cd "$_" || exit

# decompress with lz4cat
lz4cat "${INITRAMFS_FILE[@]}" | cpio -id ${CONF_FILE}  || exit 1

# Check prepare-root.conf
COMPOSEFS_ENABLED=$(awk -F '=' '/\[composefs\]/{a=1}a==1&&$1~/enabled/{print $2;exit}' ${CONF_FILE})
if [ "$COMPOSEFS_ENABLED" == 'signed' ]; then
   echo "Image is signed with composefs!"
   RC=0
else
  printf "Image is unsigned with composefs.\n
          Please using signed image for this testing!"
  RC=1
fi

# Delete temporary files
cd "$HOME" && rm -rf /tmp/initrd
exit $RC
