#!/bin/bash

set -e

# The minimal image does NOT contain podman, this test should be skipped 
if [[ "${IMAGE_NAME}" == "minimal" ]]; then
   echo "INFO : ${IMAGE_NAME} image doesn't support podman!"
   echo "PASS : $(basename "$0" .sh)"
   exit 0
fi

# Check podman is installed.
if ! rpm -q podman > /dev/null; then
   echo "INFO : This image has something wrong, podman isn't installed."
   echo "FAIL: $(basename "$0" .sh)"
   exit 1 
fi

# Please note: The default RHIOVS image contains the podman package installed but the
# service is NOT enabled by default. This is the expected behaviour.
if [ "$(systemctl is-enabled podman)" == "disabled" ]; then
   echo "INFO : Podman service is 'not enabled' in RHIVOS image"
   echo "PASS : $(basename "$0" .sh)"
   exit 0
else
   echo "INFO : Podman service is !!ENABLED!! in RHIVOS image"
   echo "FAIL : $(basename "$0" .sh)"
   exit 1
fi
